'use strict';

import MainCtrl from './main/main.controller';
import LoginCtrl from './login/login.controller';
import HomeCtr from './main/home/home.controller';
import RegisterCtrl from './login/register.controller';


angular.module('chatter', ['ionic', 'ngAnimate', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'ngMaterial', 'firebase'])
    .controller('MainCtrl', MainCtrl)
    .controller('LoginCtrl', LoginCtrl)
    .controller('HomeCtr', HomeCtr)
    .controller('RegisterCtrl', RegisterCtrl)

.run(['$rootScope', '$state', '$firebaseAuth', function($rootScope, $state, $firebaseAuth) {
    $rootScope.dataBase = new Firebase('https://blinding-inferno-9842.firebaseio.com/');
    $rootScope.auth = $firebaseAuth($rootScope.dataBase);

    function authDataCallback(authData) {
        if (authData) {
            console.log("User " + authData.uid + " is logged in with " + authData.provider);
            $rootScope.user = authData;
            $rootScope.$broadcast('userLogged',{});
            //console.log(authData);
        } else {
            $rootScope.user = null;
            console.log("User is logged out");
            $state.go('login');
        }
    }
    $rootScope.auth.$onAuth(authDataCallback);
    $rootScope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams) {
            console.log(toState.name);
            if (toState.name !== 'login') {
                if (!$rootScope.user) {
                    if (toState.name !== 'register') {
                        $state.go('login');
                    }
                }
            }
        });
}])


.factory("Auth", ["$firebaseAuth",
    function($firebaseAuth) {
        var ref = new Firebase("https://docs-sandbox.firebaseio.com", "example3");
        return $firebaseAuth(ref);
    }
])

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('main', {
            url: '/main',
            abstract: true,
            templateUrl: 'app/main/main.html',
            controller: 'MainCtrl',
        }).state('main.home', {
            url: '/home',
            cache: false,
            views: {
                'viewContent': {
                    templateUrl: 'app/main/home/home.html',
                    controller: 'HomeCtr'
                }
            }
        })
        .state('login', {
            url: '/login',
            cache: false,
            templateUrl: 'app/login/login.html',
            controller: 'LoginCtrl'
        }).state('register', {
            url: '/reg',
            cache: false,
            templateUrl: 'app/login/register.html',
            controller: 'RegisterCtrl'
        });

    $urlRouterProvider.otherwise('/login');
});
