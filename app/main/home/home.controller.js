'use strict';

class HomeCtrl {
    constructor($scope, $ionicScrollDelegate, $rootScope, $state, $ionicHistory, $firebaseObject) {
        var dataBase = new Firebase('https://blinding-inferno-9842.firebaseio.com/');
        var chatBase = dataBase.child('/posts');
        $scope.posts = $firebaseObject(chatBase);
        // var connectedRef = new Firebase("https://blinding-inferno-9842.firebaseio.com/.info/connected");
        if (!$rootScope.user) {
            $ionicHistory.nextViewOptions({
                disableAnimate: true,
                disableBack: true
            });
            $state.go('login');
        }

        function safeApply(scope, fn) {
            (scope.$$phase || scope.$root.$$phase) ? fn: scope.$apply(fn);
        }

        function clearInput() {
            $scope.input.mesaj = '';
        }

        chatBase.on('child_added', function() {
            $ionicScrollDelegate.scrollBottom(true);
        });

        $scope.input = {
            mesaj: '',
            placeholder: 'scrie mesaje (бесплатно без регистраций и смс)'
        };

        $scope.sendMessage = function(input) {
            if (input.mesaj.trim().length >= 2) {
                chatBase.push({
                    msg: input.mesaj.trim(),
                    username: $rootScope.user.password.email,
                    userid: $rootScope.user.uid
                });
                safeApply($scope, clearInput());
            }
        };
        $scope.delPost = function(key) {
            chatBase.child(key).remove();
        };

    }
}

HomeCtrl.$inject = ['$scope', '$ionicScrollDelegate', '$rootScope', '$state', '$ionicHistory', '$firebaseObject'];

export default HomeCtrl;
