'use strict';

class MainCtrl {
    constructor($scope, $rootScope) {
        $scope.logOut = function() {
            $rootScope.auth.$unauth();
        }
    }
}

MainCtrl.$inject = ['$scope', '$rootScope'];

export default MainCtrl;
