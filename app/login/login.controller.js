'use strict';

class LoginCtrl {
    constructor($scope, $state, $rootScope) {
        $rootScope.$on('userLogged', function(e, h) {
            $state.go('main.home');
        })

        $scope.register = function() {
            $state.go('register');
        };
        $scope.logIn = function(us) {
            console.log($rootScope.auth);
            $rootScope.dataBase.authWithPassword({
                email: us.email,
                password: us.pass
            }, function(error, authData) {
                if (error) {
                    console.log(error);
                } else {
                    $state.go('main.home');
                }
            });
        };
    }
}

LoginCtrl.$inject = ['$scope', '$state', '$rootScope'];

export default LoginCtrl;
