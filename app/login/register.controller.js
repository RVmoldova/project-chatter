'use strict';

class RegisterCtrl {
    constructor($scope, $state, $rootScope) {
        $scope.goToLogin = function() {
            $state.go('login');
        };
        $scope.reg = function(us) {
            console.log(us);
            $rootScope.dataBase.createUser({
                email: us.email,
                password: us.pass
            }, function(error, userData) {
                if (error) {
                    console.log("Error creating user:", error);
                } else {
                    console.log("Successfully created user account with uid:", userData.uid);
                    $state.go('login');
                }
            });
        };
    }
}

RegisterCtrl.$inject = ['$scope', '$state', '$rootScope'];

export default RegisterCtrl;
